/// <reference types="cypress" />

import { bookApi } from '../../../support/apis/book-request.api'

describe('Books', function () {
	beforeEach(function () {
		cy.fixture(`data.${Cypress.env('server')}`).as('data')
	})

	it('should return info', function () {
		const baseUrl = this.data.host
		bookApi
			.getBooks(baseUrl)
			.then(response => {
				return response.status
			})
			.then(statusCode => {
				expect(statusCode, 'check if the status code response is 200').to.equal(200)
			})
	})

	it('should allow the user to add a book to the cart', function () {})
})
