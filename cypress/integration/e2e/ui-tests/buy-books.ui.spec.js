/// <reference types="cypress" />

describe('Buy books', function () {
	beforeEach(function () {
		cy.fixture(`data.${Cypress.env('server')}`).as('data');
	});

	context('All available books', function () {
		it('should allow the user to see the list of available books', function () {});
	});

	context('Add books', function () {
		it('should allow the user to add a book to the cart', function () {});
	});
});
