/// <reference types="cypress" />

import { utils } from '../../../support/util';
import { applicationActions } from '../../../support/actions/application.actions';

describe('Authentication', function () {
	context('Register', function () {
		beforeEach(function () {
			applicationActions.navigateToLoginPage();
		});

		it('should allow the user to see all the elements in the Register Page', function () {
			cy.log('Click on the Register option');
			cy.contains('Register').click();

			cy.log('Check that all elements render on the Register page');
			cy.contains('User name').should('be.visible');
			cy.contains('Password').should('be.visible');
			cy.contains('Repeat Password').should('be.visible');
			cy.contains('Gender').should('be.visible');
			cy.contains('Address').should('be.visible');
			cy.contains('Billing Address').should('be.visible');
			cy.contains('State').should('be.visible');
			cy.contains('I agree to the Terms and Conditions.').should('be.visible');
		});

		it('should allow the user to register in the application', function () {
			cy.log('Click on the Register option');
			cy.contains('Register').click();

			cy.log('Fill out the register form');
			cy.get('#uid').type('user' + utils.timestamp());
			cy.get('#pid').type('abcd1234');
			cy.get('#pid2').type('abcd1234');
			cy.get('[value="M"]').click();
			cy.get('[type="checkbox"]').click();

			cy.log('Click on Register button and check the successful alert message');
			applicationActions.checkAlertText('[value="Register"]', 'Registered Successfully');
		});

		it('should allow the user to cancel the register', function () {
			cy.log('Click on the Register option');
			cy.contains('Register').click();
		});

		it('should allow the user to go back to the Login page', function () {});
	});

	context.skip('Login', function () {
		beforeEach(function () {
			applicationActions.navigateToLoginPage();
		});

		it('should allow the user with valid credentials access to the Books page', function () {});
	});
});
