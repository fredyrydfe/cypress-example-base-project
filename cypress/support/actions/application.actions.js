class ApplicationActions {
	checkAlertText(element, text) {
		const stub = cy.stub();
		cy.on('window:alert', stub);
		cy.get(element)
			.click()
			.then(() => {
				expect(stub.getCall(0)).to.be.calledWith(text);
			});
	}

	navigateToLoginPage() {
		cy.fixture(`data.${Cypress.env('server')}`).as('data');
		cy.get('@data').then(data => {
			cy.log('baseUrl = ' + data.baseUrl)
			cy.visit(data.baseUrl + '/login.htm');
		});
		cy.contains('Sahi Training Site').should('be.visible');
	}
}

export const applicationActions = new ApplicationActions();
