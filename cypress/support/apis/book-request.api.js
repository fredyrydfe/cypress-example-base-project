class BookApi {
	getBooks(baseUrl) {
		return cy.request({
			method: 'GET',
			url: `${baseUrl}/books`,
			qs: {
				bibkeys: 'ISBN:0451526538',
				callback: 'mycallback',
			},
		});
	}
}

export const bookApi = new BookApi();
